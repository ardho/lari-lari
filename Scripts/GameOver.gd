extends Node2D

func _ready():
	Global.current_state = "GAMEOVER"
	Global.mode_state = "ENDLESS"
	pass


func _on_MenuButton_pressed():
	MusicController._on_button_pressed()
	get_tree().change_scene("res://Scenes/MainMenu.tscn")

func _on_TryAgainButton_pressed():
	MusicController._on_button_pressed()
	get_tree().change_scene("res://Scenes/Main.tscn")

func _on_LeaderboardButton_pressed():
	MusicController._on_button_pressed()
	get_tree().change_scene("res://Scenes/Leaderboard/Leaderboard.tscn")
