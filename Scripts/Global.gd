extends Node

# MAIN_MENU
# PLAY
# GAMEOVER
# COMPLETE
var current_state
# ENDLESS
# EASY
# MEDIUM
# HARD
var mode_state
# is already submitted score
var is_submitted
var is_submitted_easy
var is_submitted_medium
var is_submitted_hard
var is_moving
var is_sliding

var level = 1
var ready

# playing state
var is_falling
var current_speed

# leaderboard attrib 
var username = null
var score
var current_rank
var current_rank_display
var spec_score = 0
var spec_score_time = 0 # dalam sec, 83939289 sec
var score_time = "00:00:00"# jam menit detik, 92:19:03
var is_level_success
var dist_with_target

# save last attrib
var last_username
var last_score
var last_current_rank
var last_spec_score
# =
var last_username_easy
var last_score_time_easy
var last_spec_score_time_easy = 0
var last_current_rank_easy
# =
var last_username_medium
var last_score_time_medium
var last_spec_score_time_medium = 0
var last_current_rank_medium
# =
var last_username_hard
var last_score_time_hard
var last_spec_score_time_hard = 0
var last_current_rank_hard

func _ready():
	pass

func set_current_rank_display(rank):
	var ret
	
	if (rank == 1):
		ret = "Congrats! you're in 1st place on the leaderboard"
	elif (rank == 2):
		ret = "Goodgame! you're in 2nd place on the leaderboard"
	elif (rank == 3):
		ret = "Wellplayed! you're in 3rd place on the leaderboard"
	else:
		ret = "You're in " + str(rank) + "th place on the leaderboard"
		
	Global.current_rank_display = ret

func set_score(point):
	if is_falling:
		pass
	else:
		point *= (1+level*0.1)
		
		spec_score = point
		score = score_format(point)

func score_format(point):
	var sep = str(point).split('.')
	if (sep.size() > 1):
		return comma_sep(sep[0]) + '.' + sep[1].left(2)
	else:
		return comma_sep(sep[0]) + '.00'

func comma_sep(number):
	var string = str(number)
	var mod = string.length() % 3
	var res = ""
	
	for i in range(0, string.length()):
		if i != 0 && i % 3 == mod:
			res += ","
		res += string[i]

	return res

