extends KinematicBody2D

var velocity = Vector2()
var is_dashing
var is_sliding
var is_start
var is_grounded
var is_can_stand
var is_ceiling
var is_dropping

var curr_pos_x
var frame

var RUN_SPEED = 1600
var JUMP_SPEED = RUN_SPEED*19/16

var time_start = 0
var time_now = 0

const GRAVITY = 98

onready var normal_collision = $NormalCollision
onready var normal_ground_raycast = $NormalGroundRayCast
onready var normal_ceil_raycast = $NormalCeilRayCast
onready var normal_ceil_raycast2 = $NormalCeilRayCast2

onready var slide_collision = $SlideCollision

func _ready():
	frame = 0
	
	is_start = false
	is_dashing = false
	is_sliding = false
	is_dropping = false
	curr_pos_x = self.position.x
	
	Global.current_speed = 0
	Global.is_falling = false
	Global.is_moving = false
	
	Global.is_level_success = false

# warning-ignore:unused_argument
func _process(delta):
	# state stuff
	if normal_ground_raycast.is_colliding():
		is_grounded = true
	else:
		is_grounded = false
	frame += 1
	if frame % 50 and self.position.x - curr_pos_x > 5:
		Global.is_moving = true
		curr_pos_x = self.position.x
	else:
		Global.is_moving = false

	if normal_ceil_raycast.is_colliding() or normal_ceil_raycast2.is_colliding():
		is_can_stand = false
	else:
		is_can_stand = true
	
	if normal_ceil_raycast.is_colliding():
		velocity.y = 0
	
	# press space to start the game
	if Input.is_action_just_pressed("ui_accept") and !is_start:
		is_start = true
		velocity.x = RUN_SPEED
		
		time_start = OS.get_ticks_msec()
		set_process(true)

	# animation
	if velocity.y == 0 and is_start:
		$AnimatedSprite.play("Run")
		MusicController._on_run()
	
	if velocity.x == 0 and velocity.y == 0 or !is_start:
		$AnimatedSprite.play("Idle")

	apply_gravity()
	jump()
	slide()
	dash()
	drop()

	move_and_slide(velocity)
	
	# game over state
	if self.position.y > 500:
		Global.is_falling = true
		MusicController._force_off_all_effect()
		velocity.x = 0

	if self.position.y > 2000 and $Camera2D.current and Global.is_falling:
		$Camera2D.current = false

	if !$Camera2D.current and Global.is_falling and self.position.y > 5000:
		Global.is_submitted_hard = false
		get_tree().change_scene("res://Scenes/LevelHard/GameOverHard.tscn")
	
	if is_start:
		_show_timer()
	
func apply_gravity():
	if is_grounded:
		velocity.y = 0
	else:
		velocity.y = velocity.y + GRAVITY

func jump():
	if Input.is_action_just_pressed("ui_up") and is_start and is_grounded and is_can_stand:
		velocity.x = RUN_SPEED
		velocity.y = -JUMP_SPEED
		$AnimatedSprite.play("Jump")
		MusicController._on_jump()
	
	if !is_grounded:
		$AnimatedSprite.play("Jump")

func slide():
	# kalo pencet bawah
	if Input.is_action_pressed("ui_down"):
		slide_state(true)
		$AnimatedSprite.play("Slide")

	# kalo lagi slide, mau keatas
	if is_sliding:
		# mau lepas dari slide
		if (Input.is_action_just_released("ui_down") or !Input.is_action_pressed("ui_down")) and is_can_stand:
			slide_state(false)
		elif !is_can_stand:
			slide_state(true)
			$AnimatedSprite.play("Slide")
		

func slide_state(val):
	is_sliding = val
	
	normal_collision.disabled = val
	slide_collision.disabled = !val
	
	Global.is_sliding = val
		
func dash():
	if Input.is_action_just_pressed("ui_right") and is_start and !is_dashing:
		$DashTime.start()
		$DashEffect.visible = true
		MusicController._on_dash()
		is_dashing = true
		velocity.x = RUN_SPEED*5

func drop():
	if Input.is_action_just_pressed("ui_shift") and is_start and !is_dropping:
		$DropTime.start()
		is_dropping = true
		velocity.y = RUN_SPEED*3

func _on_DropTime_timeout():
	velocity.y = 0
	$DropDelay.start()
	
func _on_DropDelay_timeout():
	is_dropping = false

func _on_DashTime_timeout():
	velocity.x = RUN_SPEED
	$DashEffect.visible = false
	$DashDelay.start()

func _on_DashDelay_timeout():
	is_dashing = false

func _show_timer():
	time_now = OS.get_ticks_msec()
	var elapsed = time_now - time_start
	var minutes = (elapsed / 60000)
	var seconds = (elapsed / 1000) % 60
	var miliseconds = int(elapsed) % 1000
	var time = ("%02d" % minutes) + (":%02d" % seconds) + ":" + str(miliseconds).left(2)
	Global.score_time = time
	Global.spec_score_time = elapsed


