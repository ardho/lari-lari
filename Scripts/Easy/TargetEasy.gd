extends KinematicBody2D

var velocity = Vector2()

var RUN_SPEED = 1500

func _ready():
	pass

# warning-ignore:unused_argument
func _process(delta):
	
	if Global.spec_score_time > 0:
		velocity.x = RUN_SPEED
		
	move_and_slide(velocity)
	
