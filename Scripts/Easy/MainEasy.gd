extends Node2D

var dist

func _ready():
	MusicController._on_play()
	Global.current_state = "PLAY"
	Global.mode_state = "EASY"
	Global.dist_with_target = 0
	Global.spec_score_time = 0
	dist = 700
	
	pass 

func _process(delta):
	
	if $Player.position.y < -300:
		$Target.rect_position.x = $Player.position.x + 300 + dist
		$Target.rect_position.y = $Player.position.y - 30
	
	$CollisionTarget.position.x = $Target.rect_position.x + 120
	$CollisionTarget.position.y = $Target.rect_position.y
	
	if ($Target.rect_position.x - ($Player.position.x + 120) > 0):
		Global.dist_with_target = ($Target.rect_position.x - ($Player.position.x + 120))/5000
	else:
		Global.dist_with_target = 0.0001
	
	if Global.spec_score_time > 0 and Global.is_moving:
		dist -= 0.7
	if !Global.is_moving:
		dist += 1
		
	pass

func _on_CollisionTarget_body_entered(body):
	if body == $Player:
		Global.is_level_success = true
		Global.is_submitted = false
		
		get_tree().change_scene("res://Scenes/LevelEasy/GameOverEasy.tscn")
