extends Node2D

var menu_music = load("res://Assets/cinematic-desert-sound.wav")
var play_music = load("res://Assets/wind-run.wav")
var pressed_button_effect = load("res://Assets/button_pressed.wav")
var run_effect = load("res://Assets/run.ogg")
var dash_effect = load("res://Assets/dash.wav")
var jump_effect = load("res://Assets/jump.ogg")

var jump_landing_effect = load("res://Assets/jumpland.wav")

var music_playing
var effect_playing
var is_sliding

# SecEffect: dash only
# ThiEffect: landing only
# ForEffect: jump only

func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if Global.current_state == "GAMEOVER":
		_force_off_all_effect()
	if effect_playing == "RUN":
		_on_run()

func _on_button_pressed():
	$Effect.stream = pressed_button_effect
	$Effect.volume_db = -5
	$Effect.play()

func _on_main_menu():
	if music_playing != "MAIN_MENU":
		$Music.stop()
		music_playing = "MAIN_MENU"
		$Music.stream = menu_music
		$Music.volume_db = -10
		$Music.play()

func _on_play():
	if music_playing != "PLAY":
		$Music.stop()
		$Transition.start()
		
func _on_run():
	if effect_playing != "RUN":
		if $Effect.stream_paused:
			$Effect.stream_paused = false
		if effect_playing == "JUMP":
			$ThiEffect.stop()
			$ThiEffect.stream = jump_landing_effect
			$ThiEffect.volume_db = 0
			$ThiEffect.play()
		effect_playing = "RUN"
		$Effect.stream = run_effect
		$Effect.volume_db = 15
		$Effect.play()
	if Global.is_sliding:
		$Effect.volume_db = -10000
	else:
		$Effect.volume_db = 15

func _on_jump():
	if effect_playing != "JUMP":
		if effect_playing == "RUN":
			$Effect.stop()
		effect_playing = "JUMP"
		$ForEffect.stream = jump_effect
		$ForEffect.volume_db = 10
		$ForEffect.play()

func _on_dash():
	$SecEffect.stop()
	$SecEffect.stream = dash_effect
	$SecEffect.volume_db = -10
	$SecEffect.play()
	
func _on_Transition_timeout():
	music_playing = "PLAY"
	$Music.stream = play_music
	$Music.volume_db = -20
	$Music.play()
	
func _force_off_all_effect():
	$Effect.stop()
	$SecEffect.stop()
	$ThiEffect.stop()
	$ForEffect.stop()
	effect_playing = "RUN"
