extends Node2D

const ScoreItem = preload("res://Scenes/Leaderboard/ScoreItem.tscn")
const ScoreItemCurrent = preload("res://Scenes/Leaderboard/ScoreItemCurrent.tscn")
const ScoreItemFirst = preload("res://Scenes/Leaderboard/ScoreItemFirst.tscn")
const ScoreItemSecond = preload("res://Scenes/Leaderboard/ScoreItemSecond.tscn")
const ScoreItemThird = preload("res://Scenes/Leaderboard/ScoreItemThird.tscn")

var current_rank

func _ready():
	if Global.current_state == "MAIN_MENU":
		Global.is_submitted = false
		Global.is_submitted_easy = false
		Global.is_submitted_medium = false
		Global.is_submitted_hard = false
	
	if Global.mode_state == "ENDLESS":
		$LeaderboardAPI.fetch_scores('', '', 15, Constant.TABLE_ID_ENDLESS)
	elif Global.mode_state == "EASY":
		$LeaderboardAPI.fetch_scores('', '', 15, Constant.TABLE_ID_EASY)
	elif Global.mode_state == "MEDIUM":
		$LeaderboardAPI.fetch_scores('', '', 15, Constant.TABLE_ID_MEDIUM)
	elif Global.mode_state == "HARD":
		$LeaderboardAPI.fetch_scores('', '', 15, Constant.TABLE_ID_HARD)

func _process(delta):
	pass

func _on_LeaderboardAPI_api_scores_fetched(data):
	if (!data):
		$MessageContainer/TextMessage.text = "NO INTERNET CONNECTION"
	else:
		$MessageContainer/TextMessage.visible = false
		
		var scores = parse_json(data)
		var rank = 0
		var rank_recently = -1
		
		for score in scores["response"]["scores"]:
			rank += 1
			
			if (rank <= 10 and rank <= scores["response"]["scores"].size()):
				add_to_leaderboard(rank, score["score"], score["guest"])
		
		# add current / last submitted submitted score 
		if (Global.is_submitted or Global.is_submitted_easy or Global.is_submitted_medium or Global.is_submitted_hard):
			if (Global.mode_state == "ENDLESS" and Global.is_submitted):
				$LeaderboardAPI.get_rank(Global.spec_score*100, Constant.TABLE_ID_ENDLESS)
				$MessageContainer/TextMessage.visible = true
				$MessageContainer/TextMessage.text = "LOADING YOUR LAST SUBMITTED SCORE . . ."
			elif (Global.mode_state == "EASY" and Global.is_submitted_easy):

				$LeaderboardAPI.get_rank(Global.spec_score_time, Constant.TABLE_ID_EASY)
				$MessageContainer/TextMessage.visible = true
				$MessageContainer/TextMessage.text = "LOADING YOUR LAST SUBMITTED SCORE . . ."
			elif (Global.mode_state == "MEDIUM" and Global.is_submitted_medium):

				$LeaderboardAPI.get_rank(Global.spec_score_time, Constant.TABLE_ID_MEDIUM)
				$MessageContainer/TextMessage.visible = true
				$MessageContainer/TextMessage.text = "LOADING YOUR LAST SUBMITTED SCORE . . ."
			elif (Global.mode_state == "HARD" and Global.is_submitted_hard):

				$LeaderboardAPI.get_rank(Global.spec_score_time, Constant.TABLE_ID_HARD)
				$MessageContainer/TextMessage.visible = true
				$MessageContainer/TextMessage.text = "LOADING YOUR LAST SUBMITTED SCORE . . ."
		# no submit, but theres prev submitted
		elif (Global.mode_state == "ENDLESS"):
			if (Global.last_score and Global.last_current_rank and Global.last_username):
				add_to_leaderboard(Global.last_current_rank, Global.last_score, Global.last_username)
		elif (Global.mode_state == "EASY"):
			if (Global.last_spec_score_time_easy > 0 and Global.last_current_rank_easy and Global.last_username_easy):
				add_to_leaderboard(Global.last_current_rank_easy, Global.last_score_time_easy, Global.last_username_easy)
		elif (Global.mode_state == "MEDIUM"):
			if (Global.last_spec_score_time_medium > 0 and Global.last_current_rank_medium and Global.last_username_medium):
				add_to_leaderboard(Global.last_current_rank_medium, Global.last_score_time_medium, Global.last_username_medium)
		elif (Global.mode_state == "HARD"):
			if (Global.last_spec_score_time_hard > 0 and Global.last_current_rank_hard and Global.last_username_hard):
				add_to_leaderboard(Global.last_current_rank_hard, Global.last_score_time_hard, Global.last_username_hard)
			
func add_to_leaderboard(rank, score, player_name, is_recent=false):
	
	var item
	
	if (rank == 1):
		item = ScoreItemFirst.instance()
	elif (rank == 2):
		item = ScoreItemSecond.instance()
	elif (rank == 3):
		item = ScoreItemThird.instance()
	elif (is_recent):
		item = ScoreItemCurrent.instance()
	else:
		item = ScoreItem.instance()
	
	item.get_node("PlayerName").text = str(rank) + str(".  ") + player_name
	
	item.get_node("Score").text = score
	item.margin_top = rank * 120
	
	$"HighScores/ScoreItemContainer".add_child(item)

func _on_BackButton_pressed():
	MusicController._on_button_pressed()
	
	if Global.current_state == "MAIN_MENU":
		get_tree().change_scene("res://Scenes/MainMenu.tscn")
	elif Global.current_state == "GAMEOVER":
		if Global.mode_state == "ENDLESS":
			get_tree().change_scene("res://Scenes/GameOver.tscn")
		elif Global.mode_state == "EASY":
			get_tree().change_scene("res://Scenes/LevelEasy/GameOverEasy.tscn")
		elif Global.mode_state == "MEDIUM":
			get_tree().change_scene("res://Scenes/LevelMedium/GameOverMedium.tscn")
		elif Global.mode_state == "HARD":
			get_tree().change_scene("res://Scenes/LevelHard/GameOverHard.tscn")

func _on_LeaderboardAPI_api_data_got_rank(data):
	if (!data):
		$MessageContainer/TextMessage.text = "NO INTERNET CONNECTION"
	else:
		$MessageContainer/TextMessage.visible = false
		current_rank = parse_json(data)
		# save
		if (Global.mode_state == "ENDLESS"):
			Global.last_current_rank = current_rank["response"]["rank"]
			add_to_leaderboard(current_rank["response"]["rank"], Global.score + " M", Global.username, true)
		elif (Global.mode_state == "EASY"):
			Global.last_current_rank_easy = current_rank["response"]["rank"]
			add_to_leaderboard(current_rank["response"]["rank"], Global.score_time, Global.username, true)
		elif (Global.mode_state == "MEDIUM"):
			Global.last_current_rank_medium = current_rank["response"]["rank"]
			add_to_leaderboard(current_rank["response"]["rank"], Global.score_time, Global.username, true)
		elif (Global.mode_state == "HARD"):
			Global.last_current_rank_hard = current_rank["response"]["rank"]
			add_to_leaderboard(current_rank["response"]["rank"], Global.score_time, Global.username, true)
				
