extends Node2D

const G_INIT = preload("res://Scenes/Grounds/GroundInit.tscn")
const G_SLIDE = preload("res://Scenes/Grounds/GroundSlide.tscn")
const G_STRAIGHT = preload("res://Scenes/Grounds/GroundStraight.tscn")
const G_JUMP = preload("res://Scenes/Grounds/GroundJump.tscn")
const G_SLI_JMP = preload("res://Scenes/Grounds/GroundSlideJump.tscn")

var fix_y_spawn = -1
var spawn_position = global_position

onready var player = get_parent().get_node("Player")

func _ready():
	spawn_init_ground()
	pass 

func _process(delta):
	
	# jarak player ke spawn areanya
	if spawn_position.distance_to(player.global_position) < 2000:
		
		var random_ground_type = randi() % 7
		
		if random_ground_type == 0:
			spawn_tunnel_ground()
		elif random_ground_type in [1,2,3,4]: # high chance to respawn
			spawn_straight_ground()
		elif random_ground_type == 5:
			spawn_jump_ground()
		elif random_ground_type == 6:
			spawn_slidejump_ground()
			
		# special case
		if random_ground_type != 0:
			fix_y_spawn = -1
		else:
			spawn_position.y = fix_y_spawn

func spawn_init_ground():
	var spawn_ground_instance = G_INIT.instance()
	add_child(spawn_ground_instance)
	spawn_ground_instance.global_position.x = spawn_position.x
	spawn_ground_instance.global_position.y = spawn_position.y
	
	spawn_position.x = spawn_position.x + Constant.G_INIT_W
	spawn_position.y = spawn_position.y

func spawn_tunnel_ground():
	var spawn_ground_instance = G_SLIDE.instance()
	add_child(spawn_ground_instance)
	spawn_ground_instance.global_position.x = spawn_position.x
	spawn_ground_instance.global_position.y = spawn_position.y
	
	spawn_position.x = spawn_position.x + Constant.G_SL_W
	spawn_position.y = spawn_position.y + 10
	
	fix_y_spawn = spawn_position.y

func spawn_straight_ground():
	var spawn_ground_instance = G_STRAIGHT.instance()
	add_child(spawn_ground_instance)
	spawn_ground_instance.global_position.x = spawn_position.x
	spawn_ground_instance.global_position.y = spawn_position.y
	
	spawn_position.x = spawn_position.x + Constant.G_STR_W
	spawn_position.y = \
		rand_range(Constant.GROUND_POS_Y_MIN, Constant.GROUND_POS_Y_MAX)

func spawn_jump_ground():
	var spawn_ground_instance = G_JUMP.instance()
	add_child(spawn_ground_instance)
	spawn_ground_instance.global_position.x = spawn_position.x
	spawn_ground_instance.global_position.y = spawn_position.y
	
	spawn_position.x = spawn_position.x + Constant.G_JMP_W
	spawn_position.y = spawn_position.y

func spawn_slidejump_ground():
	var spawn_ground_instance = G_SLI_JMP.instance()
	add_child(spawn_ground_instance)
	spawn_ground_instance.global_position.x = spawn_position.x
	spawn_ground_instance.global_position.y = spawn_position.y
	
	spawn_position.x = spawn_position.x + Constant.G_SJ_W
	spawn_position.y = spawn_position.y
