extends StaticBody2D

onready var player = get_parent().get_parent().get_node("Player")

func _ready():
	pass 

func _process(delta):
	# remove ground if distance with player is 10000
	if global_position.distance_to(player.global_position) > 10000 and player.global_position.x > global_position.x:
		queue_free()
