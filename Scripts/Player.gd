extends KinematicBody2D

var velocity = Vector2()
var is_dashing
var is_sliding
var is_start
var is_grounded
var is_can_stand
var is_ceiling
var is_dropping

var RUN_SPEED = 1500
var JUMP_SPEED = RUN_SPEED*19/15
const GRAVITY = 98

onready var normal_collision = $NormalCollision
onready var normal_ground_raycast = $NormalGroundRayCast
onready var normal_ceil_raycast = $NormalCeilRayCast
onready var normal_ceil_raycast2 = $NormalCeilRayCast2

onready var slide_collision = $SlideCollision

func _ready():
	is_start = false
	is_dashing = false
	is_sliding = false
	is_dropping = false
	
	Global.current_speed = 0
	Global.is_falling = false
	Global.set_score(0.00)

# warning-ignore:unused_argument
func _process(delta):
	# state stuff
	if normal_ground_raycast.is_colliding():
		is_grounded = true
	else:
		is_grounded = false

	if normal_ceil_raycast.is_colliding() or normal_ceil_raycast2.is_colliding():
		is_can_stand = false
	else:
		is_can_stand = true
	
	if normal_ceil_raycast.is_colliding():
		velocity.y = 0
	
	# press space to start the game
	if Input.is_action_just_pressed("ui_accept") and !is_start:
		is_start = true
		velocity.x = RUN_SPEED
	
	# update score
	if is_start and self.position.x > 0:
		Global.set_score(self.position.x/5000)

	# animation
	if velocity.y == 0 and is_start:
		$AnimatedSprite.play("Run")
		MusicController._on_run()
	
	if velocity.x == 0 and velocity.y == 0 or !is_start:
		$AnimatedSprite.play("Idle")

	apply_gravity()
	jump()
	slide()
	dash()
	drop()
	update_speed()

	move_and_slide(velocity)
	
	# game over state
	if self.position.y > 500:
		Global.is_falling = true
		MusicController._force_off_all_effect()
		velocity.x = 0
	

	if self.position.y > 2000 and $Camera2D.current and Global.is_falling:
		$Camera2D.current = false

	if !$Camera2D.current and Global.is_falling and self.position.y > 5000:
		Global.is_submitted = false
		get_tree().change_scene("res://Scenes/GameOver.tscn")
	
func apply_gravity():
	if is_grounded:
		velocity.y = 0
	else:
		velocity.y = velocity.y + GRAVITY

func jump():
	if Input.is_action_just_pressed("ui_up") and is_start and is_grounded and is_can_stand:
		velocity.x = RUN_SPEED
		velocity.y = -JUMP_SPEED
		$AnimatedSprite.play("Jump")
		MusicController._on_jump()
	
	if !is_grounded:
		$AnimatedSprite.play("Jump")

func slide():
	# kalo pencet bawah
	if Input.is_action_pressed("ui_down"):
		slide_state(true)
		$AnimatedSprite.play("Slide")

	# kalo lagi slide, mau keatas
	if is_sliding:
		# mau lepas dari slide
		if (Input.is_action_just_released("ui_down") or !Input.is_action_pressed("ui_down")) and is_can_stand:
			slide_state(false)
		elif !is_can_stand:
			slide_state(true)
			$AnimatedSprite.play("Slide")
		

func slide_state(val):
	is_sliding = val
	
	normal_collision.disabled = val
	slide_collision.disabled = !val
	
	Global.is_sliding = val
		
func dash():
	if Input.is_action_just_pressed("ui_right") and is_start and !is_dashing:
		$DashTime.start()
		$DashEffect.visible = true
		MusicController._on_dash()
		is_dashing = true
		velocity.x = RUN_SPEED*5

func drop():
	if Input.is_action_just_pressed("ui_shift") and is_start and !is_dropping:
		$DropTime.start()
		is_dropping = true
		velocity.y = RUN_SPEED*3

func _on_DropTime_timeout():
	velocity.y = 0
	$DropDelay.start()
	
func _on_DropDelay_timeout():
	is_dropping = false

func _on_DashTime_timeout():
	velocity.x = RUN_SPEED
	$DashEffect.visible = false
	$DashDelay.start()

func _on_DashDelay_timeout():
	is_dashing = false
	
func update_speed():
	var curr_pos = self.position.x/5000
	if curr_pos > 100:
		RUN_SPEED = 2500
		$AnimatedSprite.speed_scale = 8
	elif curr_pos > 90:
		RUN_SPEED = 2400
	elif curr_pos > 80:
		RUN_SPEED = 2300
	elif curr_pos > 70:
		RUN_SPEED = 2200
	elif curr_pos > 60:
		RUN_SPEED = 2100
		$AnimatedSprite.speed_scale = 7
	elif curr_pos > 50:
		RUN_SPEED = 2000
	elif curr_pos > 40:
		RUN_SPEED = 1900
	elif curr_pos >  30:
		RUN_SPEED = 1800
		$AnimatedSprite.speed_scale = 6
	elif curr_pos >  20:
		RUN_SPEED = 1700
	elif curr_pos >  10:
		RUN_SPEED = 1600 
	Global.current_speed = float(RUN_SPEED)/5000
