extends Node

# == Ground Property ==

const G_INIT_W = 8600	# Ground Init
const G_SL_W = 3400		# Ground Slide
const G_STR_W = 2000		# Ground Straight
const G_JMP_W = 4000		# Ground Jump
const G_SJ_W = 3400		# Ground Slide Jump

# == Ground Property ==

const GROUND_WIDTH = 128
const GROUND_NUMBER = 8
const GROUND_LENGTH = GROUND_WIDTH * GROUND_NUMBER

# == BOUNDARIES RANDOM Y POS ==
const GROUND_POS_Y_MIN = -180
const GROUND_POS_Y_MAX = 180

# TABLE ID
const TABLE_ID_ENDLESS = 557849
const TABLE_ID_EASY = 557842
const TABLE_ID_MEDIUM = 558643
const TABLE_ID_HARD = 558644
