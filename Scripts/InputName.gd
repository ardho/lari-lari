extends Control

var tmp

func _ready():
	if Global.last_username:
		$NameScoreInput.text = Global.last_username
	if Global.is_submitted and Global.mode_state == "ENDLESS" or \
		Global.is_submitted_easy and Global.mode_state == "EASY" or \
		Global.is_submitted_medium and Global.mode_state == "MEDIUM" or \
		Global.is_submitted_hard and Global.mode_state == "HARD" :
		$SubmitScoreButton.visible = false
		$NameScoreInput.text = ""
		$NameScoreInput.placeholder_text = "Submitted"
		$NameScoreInput.rect_position.x += 20
		$NameScoreInput.editable = false

func _input(event):
	if Input.is_key_pressed(KEY_ENTER):
		MusicController._on_button_pressed()
		_on_submit()

func _on_handle_empty():
	if $NameScoreInput.text.length() == 0:
		$SimpleTimer.start()
		$NameScoreInput.placeholder_text = "cannot empty!"
		return false
	if $NameScoreInput.text.length() > 16:
		$SimpleTimer2.start()
		tmp = $NameScoreInput.text
		$NameScoreInput.text = ""
		$NameScoreInput.placeholder_text = "too long!"
		return false
	return true

func _on_SimpleTimer_timeout():
	$NameScoreInput.placeholder_text = "your name . . ."

func _on_SimpleTimer2_timeout():
	$NameScoreInput.text = tmp
	$NameScoreInput.placeholder_text = "your name . . ."

func _on_SubmitScoreButton_pressed():
	MusicController._on_button_pressed()
	_on_submit()

func _on_submit():
	if !_on_handle_empty():
		pass
	else:
		# save
		if (Global.mode_state == "ENDLESS"):
			Global.last_username = $NameScoreInput.text
			Global.last_score = Global.score + " M"
			Global.last_spec_score = Global.spec_score
		elif (Global.mode_state == "EASY"):
			Global.last_username_easy = $NameScoreInput.text
			Global.last_score_time_easy = Global.score_time
			Global.last_spec_score_time_easy = Global.spec_score
		elif (Global.mode_state == "MEDIUM"):
			Global.last_username_medium = $NameScoreInput.text
			Global.last_score_time_medium = Global.score_time
			Global.last_spec_score_time_medium = Global.spec_score
		elif (Global.mode_state == "HARD"):
			Global.last_username_hard = $NameScoreInput.text
			Global.last_score_time_hard = Global.score_time
			Global.last_spec_score_time_hard = Global.spec_score
		
		Global.username = $NameScoreInput.text
		$SubmitScoreButton.text = "submitting . . ."
		$SubmitScoreButton.disabled = true
		if (Global.mode_state == "ENDLESS"):
			$GameJoltAPI.add_score(Global.score + " M", int(Global.spec_score*100), '', '', Global.username, Constant.TABLE_ID_ENDLESS)
		elif (Global.mode_state == "EASY"):
			$GameJoltAPI.add_score(Global.score_time, int(Global.spec_score_time), '', '', Global.username, Constant.TABLE_ID_EASY)
		elif (Global.mode_state == "MEDIUM"):
			$GameJoltAPI.add_score(Global.score_time, int(Global.spec_score_time), '', '', Global.username, Constant.TABLE_ID_MEDIUM)
		elif (Global.mode_state == "HARD"):
			$GameJoltAPI.add_score(Global.score_time, int(Global.spec_score_time), '', '', Global.username, Constant.TABLE_ID_HARD)
	
func _on_RequestTimer_timeout():
	pass

func _on_GameJoltAPI_api_scores_added(success):
	if (success): # response sukses
		if (Global.mode_state == "ENDLESS"):
			get_tree().change_scene("res://Scenes/Leaderboard/Leaderboard.tscn")
			Global.is_submitted = true
		elif (Global.mode_state == "EASY"):
			get_tree().change_scene("res://Scenes/Leaderboard/LeaderboardEasy.tscn")
			Global.is_submitted_easy = true
		elif (Global.mode_state == "MEDIUM"):
			get_tree().change_scene("res://Scenes/Leaderboard/LeaderboardMedium.tscn")
			Global.is_submitted_medium = true
		elif (Global.mode_state == "HARD"):
			get_tree().change_scene("res://Scenes/Leaderboard/LeaderboardHard.tscn")
			Global.is_submitted_hard = true
		
	else: # response fail
		$SimpleTimer3.start()
		$SubmitScoreButton.text = "submit score"
		$SubmitScoreButton.disabled = false
		
		tmp = $NameScoreInput.text
		$NameScoreInput.text = ""
		$NameScoreInput.placeholder_text = "no internet connection!  "

func _on_SimpleTimer3_timeout():
	$NameScoreInput.text = tmp
