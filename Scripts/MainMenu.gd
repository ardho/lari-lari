extends Control

# mode select var
var mode = ["Endless Mode", "Easy", "Medium", "Hard"]
var pointer = 0

func _ready():
	MusicController._on_main_menu()
	Global.current_state = "MAIN_MENU"
	_set_global_mode_state()
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func _on_StartButton_pressed():
	MusicController._on_button_pressed()
	if (pointer == 0):
		get_tree().change_scene("res://Scenes/Main.tscn")
	elif (pointer == 1):
		get_tree().change_scene("res://Scenes/LevelEasy/MainEasy.tscn")
	elif (pointer == 2):
		get_tree().change_scene("res://Scenes/LevelMedium/MainMedium.tscn")
	elif (pointer == 3):
		get_tree().change_scene("res://Scenes/LevelHard/MainHard.tscn")
	


func _on_LeaderboardButton_pressed():
	MusicController._on_button_pressed()
	if (pointer == 0):
		get_tree().change_scene("res://Scenes/Leaderboard/Leaderboard.tscn")
	elif (pointer == 1):
		get_tree().change_scene("res://Scenes/Leaderboard/LeaderboardEasy.tscn")
	elif (pointer == 2):
		get_tree().change_scene("res://Scenes/Leaderboard/LeaderboardMedium.tscn")
	elif (pointer == 3):
		get_tree().change_scene("res://Scenes/Leaderboard/LeaderboardHard.tscn")


func _on_ModeKanan_pressed():
	MusicController._on_button_pressed()
	pointer += 1
	if (pointer >= 4):
		pointer = 0
	$Mode.text = mode[pointer]
	_set_global_mode_state()


func _on_ModeKiri_pressed():
	MusicController._on_button_pressed()
	pointer -= 1
	if (pointer <= -1):
		pointer = 3
	$Mode.text = mode[pointer]
	_set_global_mode_state()

func _set_global_mode_state():
	if pointer == 0:
		Global.mode_state = "ENDLESS"
	elif pointer == 1:
		Global.mode_state = "EASY"
	elif pointer == 2:
		Global.mode_state = "MEDIUM"
	elif pointer == 3:
		Global.mode_state = "HARD"


func _on_HowToPlay_pressed():
	MusicController._on_button_pressed()
	get_tree().change_scene("res://Scenes/HowToPlay.tscn")


func _on_ExitButton_pressed():
	get_tree().quit()
